
--       ««««««««««««««««««« Marcelo Vargas »»»»»»»»»»»»»»»»»»»»
-- ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯

 --IF NOT EXISTS(SELECT * FROM DBO.SYSDATABASES WHERE NAME = 'acme')
 --    BEGIN
 --             CREATE DATABASE acme;
 --    END
	-- PRINT 'CREAR LA BD acme';
 --GO

USE acme;
go

PRINT 'Creando la tabla TYPE_CONTACT';

IF NOT EXISTS (SELECT 1 FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[type_contract]') 
		       AND type in (N'U'))
 BEGIN
	CREATE TABLE type_contract (
		id 				INT NOT NULL IDENTITY(1,1),
		created_on		DATE NOT NULL,
		updated_on 		DATE NULL DEFAULT NULL,
		version 		INT NOT NULL,
		description		VARCHAR(50) NULL ,
		responsable		VARCHAR(50) NULL ,
		type_contract   VARCHAR(50) NOT NULL,
		CONSTRAINT PK_type_contract PRIMARY KEY (id)
	);

		PRINT 'Tabla TYPE_CONTRACT creada !!!!!!!!!...';
	END
 ELSE 
	BEGIN
		PRINT 'La tabla TYPE_CONTRACT YA EXISTE en la base de datos .........';
	END
go

PRINT 'Creando la tabla  CONTRACT';

IF NOT EXISTS (SELECT 1 FROM sys.objects 
		       WHERE object_id = OBJECT_ID(N'[dbo].[contract]') 
		       AND type in (N'U'))
 BEGIN
	CREATE TABLE contract (
			id               INT NOT NULL IDENTITY(1,1),
			created_on       DATE NOT NULL,
			updated_on       DATE NULL DEFAULT NULL,
			version          INT NOT NULL,
			contract_amount  VARCHAR(9) NOT NULL,
			contract_code    VARCHAR(10) NOT NULL,
			end_date         DATE NOT NULL,
			init_date        DATE,
			payment_type     VARCHAR(9) NOT NULL,
			employee_id 	 INT NOT NULL ,    
			position_id  	 INT NOT NULL ,
			project_id       INT NOT NULL ,
			type_contract_id INT NOT NULL,
			CONSTRAINT PK_contract PRIMARY KEY (id),
	);

		PRINT 'Tabla CONTRACT creada !!!!!!!!!...';
	END
 ELSE 
	BEGIN
		PRINT 'La tabla CONTRACT YA EXISTE en la base de datos .........';
	END
go



-- Definicion de la relacion entre CONTRACT y TYPE_CONTRACT.
IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_type_contract_contract]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       CONSTRAINT [FK_type_contract_contract] FOREIGN KEY([type_contract_id])
REFERENCES [dbo].[type_contract] (id)

GO
ALTER TABLE [dbo].[contract] CHECK 
       CONSTRAINT [FK_type_contract_contract]
GO


-- -- Definicion de la relacion entre CONTRACT y EMPLOYEE.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_employee_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_employee_contract] FOREIGN KEY([employee_id])
-- REFERENCES [dbo].[employee] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_employee_contract]
-- GO


-- -- Definicion de la relacion entre CONTRACT y POSITION.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_position_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_position_contract] FOREIGN KEY([position_id])
-- REFERENCES [dbo].[position] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_position_contract]
-- GO


-- -- Definicion de la relacion entre CONTRACT y PROJECT.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_project_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_project_contract] FOREIGN KEY([project_id])
-- REFERENCES [dbo].[project] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_project_contract]
-- GO








